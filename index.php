<!--
/*************************************************
Copyright (C), 2024, 愜谷海 Wang Haiqiang
File name: photo_wall.php
Author: 愜谷海 Wang Haiqiang QQ:18850860 Version: 2.0 Date: 2024.11.13
Description: php照片墙Photo_wall程序 单文件,自适应排列显示传递目录中的所有图像文件
		并在图像下方显示文件名称(不包括扩展名)。
Others: 目录传递格式复合php目录格式，最后不加'/';
Function List: http://www.xxx.cn/photo_wall.php?photo_path=./images&photo_file_name=temp.jpg
		当前版本图像和文件名直接链接到图像，下一版本将传递图像名称由本页显示。   */
-->
<?php
//传递参数处理 递照片目录及设置默认值
	if (isset($_GET['photo_path']) && !empty($_GET['photo_path'])) {
		$photo_path = $_GET['photo_path'];    
		} 
	else {
    // 处理photo_path未设置或为空的情况
		$photo_path = './imgs';      //设置默认值，修改为自己的目录名
		 }
//传递照片文件名
	$photo_file_name = isset($_GET['photo_file_name']) ? $_GET['photo_file_name'] : '';
//建立照片数组
	if(is_dir($photo_path)) {
		$photo_files=scandir($photo_path);    //扫描目录照片建立数组
		$temp=array_splice($photo_files,0,2); //删除$photo_files里面的.和..组成$photo_files新数组
							}
	else {$photo_files='';	
		 }	
?>
<!DOCTYPE html>
<html lang="zh-cn">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="keywords" content="Photo Wall,照片墙,PHP ">
<meta name="author" content="Wang Haiqiang,QQ:18850860">
<title>PHP Photo Wall 照片墙</title>
<style>
	a {
	text-decoration: none;
		}
  #photo-wall {
    display: flex;
    flex-wrap: wrap;	
  }
  .photo {
    margin: 5px;
    width: 150px;
    height: 150px;
    object-fit: cover;
	border: 2px solid #fff;
    box-shadow: 0 0 5px rgba(0,0,0,0.3);
  }
  .txt{
	position:relative;
	margin-top: -5px;   
	vertical-align:top;		
	color:#000080;   /* 文字颜色 */
	font: 20px Arial, sans-serif;
	font-weight: bold;
  }
  #footer {
  text-align: center;
  padding: 1px;
  background-color: DarkSalmon;
  color: white;
	}
</style>
</head>
<body align='center'>	
	<h1>玉渊潭银杏树下跑团照片墙</h1>
	<div id="photo-wall">
<?php  
	if (!empty($photo_files)) {
		foreach ($photo_files as $key => $value) {
			echo "<div><a href='".$photo_path.'/'.$value."'><img class='photo' src='".$photo_path.'/'.$value."' /></a>". PHP_EOL;
			echo "<a href='".$photo_path.'/'.$value."'><p class='txt' >".substr($value,0,strrpos($value,'.'))."</p></a></div>". PHP_EOL;
											     }
							  }
	else {echo "<p class='txt' >照片未找到！请检查目录名称是否正确或目中是否有照片文件</p>";
		 }
?>  
	</div> 
<!--页脚-->
	<div id="footer">
		<p>Author: 愜谷海&nbsp&nbsp&nbsp&nbsp&nbsp
		<a href="http://www.kip.cn">KIP(Knowledge Is Power)</a></p>	
	</div>
</body>
</html>
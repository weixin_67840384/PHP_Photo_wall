# PHP_Photo_wall

php照片墙Photo_wall程序,单PHP文件实现自适应排列显示传递目录中的所有图像文件,并在图像下方显示文件名称(不包括扩展名).
Description: php照片墙Photo_wall程序 自适应排列显示传递目录中的所有图像文件
		并在图像下方显示文件名称(不包括扩展名)。
Others: 目录传递格式符合php目录格式，最后不加'/';
Function List: http://www.xxx.cn/photo_wall.php?photo_path=./images&photo_file_name=temp.jpg
		当前版本图像和文件名直接链接到图像，下一版本将传递图像名称由本页显示。
 安装:本程序设置默认照片目录为: "./imgs" 将此修改您的照片默认目录名称, 直接复制文件至您的网站运行即可.